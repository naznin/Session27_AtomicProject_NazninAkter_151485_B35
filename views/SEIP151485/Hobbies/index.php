<html>
<head>
<link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
    </head>
<?php
require_once("../../../vendor/autoload.php");
use App\Hobbies\Hobbies;
use App\Message\Message;


$hobby=new Hobbies();
$recordSet=$hobby->index("obj");
$serial=1;
echo "<table border='1px' align='center'>";
echo "<th>serial</th><th>id</th><th>Person Name</th><th>Hobbies</th><th>Action</th>";

foreach($recordSet as $oneRecord) {
    echo "<tr>";
    echo "<td>$serial</td>";
    echo "<td>$oneRecord->id</td>";
    echo "<td>$oneRecord->person_name</td>";
    echo "<td>$oneRecord->hobbies</td>";

    $serial++;
    echo "
    <td>
    <a href='view.php?id=$oneRecord->id'><button class='btn-success'>view</button></a>
    <a href='edit.php?id=$oneRecord->id'><button class='btn-primary'>edit</button></a>
     <a href='delete.php?id=$oneRecord->id'><button class='btn-info'>delete</button></a>
    </td>";
    echo "</tr>";

}
//$book->store();
?>
</html>

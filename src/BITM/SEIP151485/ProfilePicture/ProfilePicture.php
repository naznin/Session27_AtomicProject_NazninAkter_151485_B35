<?php
namespace App\ProfilePicture;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;


class ProfilePicture extends DB{
    public $id="";
    public $name="";
    public $profile_picture="";

    public function __construct(){
    parent:: __construct();
    if(!isset( $_SESSION)) session_start();
}

    public function setData($postVariableData=NULL){

        if(array_key_exists('id',$postVariableData)){
            $this->id = $postVariableData['id'];
        }

        if(array_key_exists('name',$postVariableData)){
            $this->name = $postVariableData['name'];
        }

        if(array_key_exists('image_name',$postVariableData)){
            $this->profile_picture = $postVariableData['image_name'];
        }
    }



    public function store(){

    $arrData = array( $this->name, $this->profile_picture);

    $sql = "Insert INTO profile_picture(name, profile_picture) VALUES (?,?)";
    $STH = $this->DBH->prepare($sql);

    $result = $STH->execute($arrData);

     if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
     else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');


    }// end of store method


 public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from profile_picture');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();

    public function view($fetchMode='ASSOC'){

        $STH=$this->DBH->query('SELECT * from profile_picture WHERE id='.$this->id);
        $fetchMode=strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ')>0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        $arrAllData=$STH->fetch();
        return $arrAllData;
    }//end of view()













}

?>


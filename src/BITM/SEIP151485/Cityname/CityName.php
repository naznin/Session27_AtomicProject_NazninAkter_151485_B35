<?php
namespace App\CityName;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO ;

class CityName extends DB{
    public $id="";
    public $person_name="";
    public $city_name="";

    public function __construct(){
    parent:: __construct();
    if(!isset( $_SESSION)) session_start();
}

    public function setData($postVariableData=NULL){

        if(array_key_exists('id',$postVariableData)){
            $this->id = $postVariableData['id'];
        }

        if(array_key_exists('person_name',$postVariableData)){
            $this->person_name = $postVariableData['person_name'];
        }

        if(array_key_exists('city_name',$postVariableData)){
            $this->city_name = $postVariableData['city_name'];
        }
    }



    public function store(){

    $arrData = array( $this->person_name, $this->city_name);

    $sql = "INSERT INTO city(person_name, city_name) VALUES (?,?)";
    $STH = $this->DBH->prepare($sql);

    $result = $STH->execute($arrData);

     if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
     else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');


    }// end of store method
 public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from city');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();

    public function view($fetchMode='ASSOC'){

        $STH=$this->DBH->query('SELECT * from city WHERE id='.$this->id);
        $fetchMode=strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ')>0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        $arrAllData=$STH->fetch();
        return $arrAllData;
    }//end of view()











}

?>

